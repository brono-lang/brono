use pest::Parser;
use pest::iterators::Pair;

use crate::parser::*;
use crate::BMacroArg;

pub fn rep<'a>(p: &mut Vec<Pair<'a, Rule>>, n: &Pair<'a, Rule>, a: Vec<BMacroArg>) {
    let times: usize = a.get(0).unwrap().value.trim().parse().unwrap();

    for _ in 0..times {
        p.push(n.clone());
    }
}

pub fn iter_text<'a>(p: &mut Vec<Pair<'a, Rule>>, n: &Pair<'a, Rule>, a: Vec<BMacroArg>) {
    for (i, c) in a.get(0).unwrap().value.chars().enumerate() {
        if i == 0 || i == a.get(0).unwrap().value.chars().into_iter().collect::<Vec<char>>().len() - 1 {
            continue;
        }

        let mut lines = vec![
            format!("const {} {};", a.get(1).unwrap().value, c as u8),
            format!("{}", n.as_str()),
        ];

        let const_code = Box::leak(Box::new(lines.remove(0)));
        let n_code = Box::leak(Box::new(lines.remove(0)));

        p.push(BronoParser::parse(Rule::k_const, const_code).unwrap().next().unwrap());
        p.push(BronoParser::parse(n.as_rule(), n_code).unwrap().next().unwrap());
    }
}
