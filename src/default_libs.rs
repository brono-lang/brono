pub const STD_BOILERPLATE: &str = "
macro inc(times:number) #{
    #[rep<${times}>]
    bf +;
}#
macro dec(times:number) #{
    #[rep<${times}>]
    bf -;
}#
macro ptr_left(times:number) #{
    #[rep<${times}>]
    bf <;
}#
macro ptr_right(times:number) #{
    #[rep<${times}>]
    bf >;
}#
macro cin(times:number) #{
    #[rep<${times}>]
    bf ,;
}#
macro cout(times:number) #{
    #[rep<${times}>]
    bf .;
}#
macro times_fn_macro(name:ident) #{
    fn ${name}(times) {
        ${name}!(times);
    }
}#
";

pub const STD: &str = "
import std_boilerplate;

times_fn_macro!(inc);
times_fn_macro!(dec);
times_fn_macro!(ptr_left);
times_fn_macro!(ptr_right);
times_fn_macro!(cin);
times_fn_macro!(cout);

macro print(msg:text) #{
	loop { bf -; }
	const p 0;

	#[iter_text<${msg}, o>]
    {
	    if (o > p) {
	    	const x (o - p);
	    	inc!(x);
	    	bf .;
	    }

	    elif (o == p) {
	    	bf .;
	    }

	    else {
	    	const x (p - o);
	    	dec!(x);
	    	bf .;
	    }

	    reconst p (o - 0);
    }
}#

macro println(msg:text) #{
    print!(${msg});
    print!(\"\n\");
}#
";
