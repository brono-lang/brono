/*

POSSIBLE BUGS:
    Nested blocks skipping lines due to self.skip_next.

*/

mod parser;
mod messages;
mod b_macro_func;
mod default_libs;

use pest::Parser;
use pest::iterators::{ Pair, Pairs };
use hashbrown::HashMap;

use parser::*;
use messages::*;

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum BMacro {
    Args(String, Vec<BMacroArg>),
    NoArgs(String),
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct BMacroArg {
    kind: BMacroArgType,
    value: String,
}

impl BMacroArg {
    pub fn new(value: String, kind: BMacroArgType) -> Self {
        return Self {
            value,
            kind,
        };
    }
}

#[derive(Debug, Eq, PartialEq, Clone, Copy)]
pub enum BMacroArgType {
    Ident,
    Number,
    String,
}

#[derive(Debug, Eq, PartialEq, Clone, Copy)]
pub enum MacroArgType {
    Nut, // Just an internal joke! :)
    Ident,
    Text,
    Number,
    Block,
}

impl MacroArgType {
    pub fn from_str(arg_type: &str) -> Option<Self> {
        return Some(match arg_type.trim() {
            "ident" => MacroArgType::Ident,
            "text" => MacroArgType::Text,
            "number" => MacroArgType::Number,
            "block" => MacroArgType::Block,
            _ => return None,
        });
    }

    pub fn from_rule(arg_type: Rule) -> Option<Self> {
        return Some(match arg_type {
            Rule::ident => MacroArgType::Ident,
            Rule::string => MacroArgType::Text,
            Rule::number => MacroArgType::Number,
            Rule::block => MacroArgType::Block,
            _ => return None,
        });
    }
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Compiler<'a> {
    main: (&'a str, &'a str),
    consts: HashMap<String, HashMap<String, u8>>,
    sources: HashMap<&'a str, (&'a str, Vec<Pair<'a, Rule>>)>,
    imports: Vec<&'a str>,
    functions: HashMap<String, (Vec<String>, Vec<Pair<'a, Rule>>)>,
    macros: HashMap<String, (Vec<(String, MacroArgType)>, String)>,
    bf_out: Vec<char>,
    skip_next: bool,
}

impl<'a> Compiler<'a> {
    pub fn new(main_file_name: &'a str, main_code: &'a str) -> Self {
        let mut compiler = Self {
            main: (main_file_name, main_code),
            consts: HashMap::new(),
            sources: HashMap::new(),
            imports: Vec::new(),
            functions: HashMap::new(),
            macros: HashMap::new(),
            bf_out: Vec::new(),
            skip_next: false,
        };

        compiler.add_source(None, "std_boilerplate", default_libs::STD_BOILERPLATE).unwrap();
        compiler.add_source(None, "std", default_libs::STD).unwrap();

        return compiler;
    }

    pub fn add_source(&mut self, file_name: Option<&str>, name: &'a str, code: &'a str) -> Result<(), ()> {
        let file_name: String = match file_name {
            Some(s) => s.to_string(),
            None => format!("lib: {name}"),
        };

        let file_name_static = Box::leak(Box::new(file_name));

        let ast = match BronoParser::parse(Rule::file, code) {
            Ok(o) => o,
            Err(e) => {
                Self::parser_error(e, file_name_static);

                return Err(());
            },
        };

        self.sources.insert(name, (file_name_static, ast.into_iter().collect()));

        return Ok(());
    }

    fn parser_error(error: pest::error::Error<Rule>, file: &'a str) {
        let msg = format!("Failed to parse code: {:#?}", error);
        let message = Message::new(&msg, MessageType::Error, MessageLocation::File(file, MessageSpan::empty(), None));
        message.print();
    }

    fn import_code(&mut self, library: &str) -> Result<(), ()> {
        // I need to figure out how to not clone an entire frickin' AST!
        let source = self.sources.get(library).unwrap().clone();

        self.process_file(&source.1, source.0)?;

        return Ok(());
    }

    pub fn compile(&mut self) -> Option<String> {
        let ast = match BronoParser::parse(Rule::file, self.main.1) {
            Ok(o) => o,
            Err(e) => {
                Self::parser_error(e, self.main.0);

                return None;
            },
        };

        match self.process_file(&ast.into_iter().collect(), self.main.0) {
            Ok(_) => (),
            Err(_) => return None,
        };

        let main_func = match self.functions.get("main") {
            Some(s) => s.clone(),
            None => {
                let message = Message::new(
                    "Missing main() function!",
                    MessageType::Error,
                    MessageLocation::File(self.main.0, MessageSpan::empty(), None),
                );

                message.print();

                return None;
            },
        };

        if main_func.0.len() != 0 {
            let message = Message::new(
                "main() function doesn't take any parameters! It is the program entry!",
                MessageType::Error,
                MessageLocation::File(self.main.0, MessageSpan::empty(), None),
            );

            message.print();

            return None;
        }

        match self.process_function(self.main.0, "main", &Vec::new(), &main_func.1) {
            Ok(_) => (),
            Err(_) => return None,
        };

        return Some(self.bf_out.iter().map(|x| x.to_string()).collect::<Vec<_>>().concat());
    }

    fn process_file(&mut self, ast: &Vec<Pair<'a, Rule>>, file_name: &str) -> Result<(), ()> {
        // This literally makes no sense. (It just gets the file contents.)
        for (file_i, file) in ast.iter().enumerate() {
            // Now we're talkin'!
            for record in file.clone().into_inner() {
                match record.as_rule() {
                    Rule::k_const => {
                        let record_str = record.as_str();
                        let record_span = record.as_span();

                        let pairs: Vec<_> = record.into_inner().into_iter().collect();

                        let name = pairs.get(0).unwrap().as_str();
                        let value: u8 = pairs.get(1).unwrap().as_str().trim().parse().unwrap();

                        if self.get_const("", name) == None {
                            self.register_const("", name, value);
                        }

                        else {
                            let msg = format!("Cannot create global const more than once! (Duplicate.)");
                            let message = Message::new(
                                &msg,
                                MessageType::Error,
                                MessageLocation::File(
                                    file_name,
                                    MessageSpan::new(record_str, record_span.start()),
                                    Some((record_span.start(), record_span.end() - 1)),
                                ),
                            );
                            message.print();

                            return Err(());
                        }
                    },
                    Rule::k_import => {
                        let record_str = record.as_str();
                        let record_span = record.as_span();

                        let pairs: Vec<_> = record.into_inner().into_iter().collect();

                        let library = pairs.get(0).unwrap().as_str();

                        if self.sources.contains_key(library) {
                            match self.import_code(library) {
                                Ok(_) => (),
                                Err(_) => return Err(()),
                            };

                            if self.imports.contains(&library) == false {
                                self.imports.push(library);
                            }

                            else {
                                let msg = format!("Duplicate import! (LIB: '{library}')");
                                let message = Message::new(&msg, MessageType::Error, MessageLocation::File(
                                    file_name,
                                    MessageSpan::new(record_str, record_span.start()),
                                    Some((pairs.get(0).unwrap().as_span().start(), pairs.get(0).unwrap().as_span().end())),
                                ));
                                message.print();

                                return Err(());
                            }
                        }

                        else {
                            let msg = format!("Invalid import! (LIB: '{library}')");
                            let message = Message::new(&msg, MessageType::Error, MessageLocation::File(
                                file_name,
                                MessageSpan::new(record_str, record_span.start()),
                                Some((pairs.get(0).unwrap().as_span().start(), pairs.get(0).unwrap().as_span().end())),
                            ));
                            message.print();

                            return Err(());
                        }
                    },
                    Rule::k_fn => {
                        let record_str = record.as_str();
                        let record_span = record.as_span();

                        let pairs: Vec<_> = record.into_inner().into_iter().collect();

                        let name = pairs.get(0).unwrap().as_str();

                        let arguments_pre = pairs.get(1).unwrap().clone().into_inner();
                        let mut arguments: Vec<String> = Vec::new();
                        for i in arguments_pre {
                            arguments.push(i.as_str().to_string());
                        }

                        let block = pairs.get(2).unwrap().clone();

                        if self.functions.contains_key(name) == false {
                            self.functions.insert(name.to_string(), (arguments, block.into_inner().into_iter().collect()));
                        }

                        else {
                            let msg = format!("Duplicate function definition: '{name}'");
                            let message = Message::new(
                                &msg,
                                MessageType::Error,
                                MessageLocation::File(
                                    file_name,
                                    MessageSpan::new(record_str, record_span.start()),
                                    Some((record_span.start(), record_span.end())),
                                ),
                            );
                            message.print();

                            return Err(());
                        }
                    },
                    Rule::k_macro => {
                        let record_str = record.as_str();
                        let record_span = record.as_span();

                        let pairs: Vec<_> = record.into_inner().into_iter().collect();

                        let name = pairs.get(0).unwrap().as_str();

                        let arguments_pre = pairs.get(1).unwrap().clone().into_inner();
                        let mut arguments: Vec<(String, MacroArgType)> = Vec::new();
                        let mut carg = "";
                        for (i, a) in arguments_pre.enumerate() {
                            if i & 1 == 0 {
                                carg = a.as_str();
                            }

                            else {
                                arguments.push((carg.to_string(), MacroArgType::from_str(a.as_str()).unwrap()));
                            }
                        }

                        let block = pairs.get(2).unwrap().clone();

                        if self.macros.contains_key(name) == false {
                            let mut macro_code = String::new();

                            for i in block.into_inner().into_iter() {
                                macro_code.push_str(i.as_str());
                            }

                            self.macros.insert(name.to_string(), (arguments, macro_code));
                        }

                        else {
                            let msg = format!("Duplicate macro definition: '{name}'");
                            let message = Message::new(
                                &msg,
                                MessageType::Error,
                                MessageLocation::File(
                                    file_name,
                                    MessageSpan::new(record_str, record_span.start()),
                                    Some((record_span.start(), record_span.end())),
                                ),
                            );
                            message.print();

                            return Err(());
                        }
                    },
                    Rule::macro_call => {
                        let record_str = record.as_str();
                        let record_span = record.as_span();

                        let pairs: Vec<_> = record.into_inner().into_iter().collect();

                        let name = pairs.get(0).unwrap().as_str();
                        let args: Vec<_> = pairs.get(1).unwrap()
                            .clone()
                            .into_inner()
                            .into_iter()
                            .map(|x| (x.clone(), x.as_rule()))
                            .map(|x| {
                                if x.1 != Rule::ident {
                                    (x.0, MacroArgType::from_rule(x.1).unwrap())
                                }

                                else {
                                    if let Some(s) = self.get_const("", x.0.as_str()) {
                                        let s_string = Box::leak(Box::new(s.to_string()));
                                        let parsed = BronoParser::parse(Rule::number, s_string).unwrap().next().unwrap();

                                        (parsed, MacroArgType::from_rule(Rule::number).unwrap())
                                    }

                                    else {
                                        (x.0, MacroArgType::from_rule(x.1).unwrap())
                                    }
                                }
                            })
                            .collect();

                        match self.macros.get(name) {
                            Some(m) => {
                                if args.len() != m.0.len() {
                                    let p_span_start = record_span.start() + name.chars().into_iter().collect::<Vec<char>>().len() + 1;

                                    let msg = format!("Expected {} arguments, but got {} instead!", m.0.len(), args.len());
                                    let message = Message::new(
                                        &msg,
                                        MessageType::Error,
                                        MessageLocation::File(file_name, MessageSpan::new(record_str, record_span.start()), Some((p_span_start, record_span.end() - 1))),
                                    );
                                    message.print();

                                    return Err(());
                                }

                                for i in 0..args.len() {
                                    if args[i].1 != m.0[i].1 {
                                        let p_span_start = record_span.start() + name.chars().into_iter().collect::<Vec<char>>().len() + 1;

                                        let msg = format!("Macro argument types are incorrect!");
                                        let message = Message::new(
                                            &msg,
                                            MessageType::Error,
                                            MessageLocation::File(file_name, MessageSpan::new(record_str, record_span.start()), Some((p_span_start, record_span.end() - 1))),
                                        );
                                        message.print();

                                        return Err(());
                                    }
                                }

                                self.expand_macro(file_name, "", &args, name)?;
                            },
                            None => {
                                let name_length = name.chars().into_iter().collect::<Vec<char>>().len();

                                let msg = format!("Non-existant macro: '{name}'");
                                let message = Message::new(
                                    &msg,
                                    MessageType::Error,
                                    MessageLocation::File(file_name, MessageSpan::new(record_str, record_span.start()), Some((record_span.start(), record_span.start() + name_length))),
                                );
                                message.print();

                                return Err(());
                            },
                        };
                    },
                    Rule::EOI => (),
                    _ => unreachable!(),
                };
            }

            if file_i > 0 {
                panic!("There should only be one file, and one file only! Fix your shit!");
            }
        }

        return Ok(());
    }

    // Expand a macro.
    fn expand_macro(&mut self, file: &str, scope: &str, arguments: &Vec<(Pair<'a, Rule>, MacroArgType)>, name: &str) -> Result<(), ()> {
        let mcro = self.macros.get(name).unwrap();

        let mut processed = mcro.1.clone();

        for ((m, _), (a, _)) in mcro.0.iter().zip(arguments) {
            let string = a.as_str();

            processed = processed.replace(&format!("{}{}{}", "${", m, "}"), &string);
        }

        let processed = Box::new({
            if scope.trim() == "" {
                processed
            } else {
                format!("{}{}{}", "{", processed, "}")
            }
        });

        let processed_str: &'static mut String = Box::leak(processed);

        let rule = {
            if scope.trim() == "" {
                Rule::file
            } else {
                Rule::block
            }
        };

        let file_mcro = format!("{file} (macro: {name})");

        let ast: Vec<_> = match brono_ast_static_str(rule, processed_str) {
            Ok(o) => o.into_iter().collect(),
            Err(e) => {
                let msg = format!("Failed to parse code in macro: '{name}'");
                let message = Message::new(
                    &msg,
                    MessageType::Error,
                    MessageLocation::File(file, MessageSpan::empty(), None)
                );
                message.print();
                let msg = format!("Error: {e:#?}");
                let message = Message::new(
                    &msg,
                    MessageType::Error,
                    MessageLocation::File(file, MessageSpan::empty(), None)
                );
                message.print();

                return Err(());
            },
        };

        match rule {
            Rule::file => self.process_file(&ast, &file_mcro),
            Rule::block => self.process_block(&file_mcro, scope, &ast[0].clone().into_inner().into_iter().collect(), false),
            _ => unreachable!(),
        }?;

        return Ok(());
    }

    fn expand_b_macro(&mut self, mcro: BMacro, file: &str, scope: &str, code_str: &str, code_span: pest::Span<'a>, next: &Pair<'a, Rule>) -> Result<(), ()> {
        let name: String;

        match mcro {
            BMacro::Args(ref n, _) => name = n.to_string(),
            BMacro::NoArgs(ref n) => name = n.to_string(),
        };

        let map = b_macro_map();

        if let Some(s) = map.get(&name) {
            if s.len() > 0 {
                if let BMacro::NoArgs(_) = mcro {
                    let msg = format!("Expected arguments!");
                    let message = Message::new(
                        &msg,
                        MessageType::Error,
                        MessageLocation::File(file, MessageSpan::new(code_str, code_span.start()), Some((code_span.start(), code_span.end()))),
                    );
                    message.print();

                    return Err(());
                }
            }

            else {
                if let BMacro::Args(_, _) = mcro {
                    let msg = format!("Expected no arguments!");
                    let message = Message::new(
                        &msg,
                        MessageType::Error,
                        MessageLocation::File(file, MessageSpan::new(code_str, code_span.start()), Some((code_span.start(), code_span.end()))),
                    );
                    message.print();

                    return Err(());
                }
            }

            if let BMacro::Args(_, ref a) = mcro {
                if a.len() != s.len() {
                    let msg = format!("Expected {} arguments, but got {}!", s.len(), a.len());
                    let message = Message::new(
                        &msg,
                        MessageType::Error,
                        MessageLocation::File(file, MessageSpan::new(code_str, code_span.start()), Some((code_span.start(), code_span.end()))),
                    );
                    message.print();

                    return Err(());
                }

                for (i, j) in a.iter().enumerate() {
                    if j.kind != *s.get(i).unwrap() {
                        let msg = format!("Invalid argument types!");
                        let message = Message::new(
                            &msg,
                            MessageType::Error,
                            MessageLocation::File(file, MessageSpan::new(code_str, code_span.start()), Some((code_span.start(), code_span.end()))),
                        );
                        message.print();

                        return Err(());
                    }
                }
            }

            let mut arguments: Vec<BMacroArg> = Vec::new();

            match mcro {
                BMacro::Args(_, a) => arguments = a,
                BMacro::NoArgs(_) => (),
            };

            let mut processed: Vec<Pair<'a, Rule>> = Vec::new();

            b_macro_func(&mut processed, next, arguments, &name);

            let file_mcro = format!("{file} (behavior macro: {name})");
            self.process_block(&file_mcro, scope, &processed, false)?;
        }

        else {
            let msg = format!("Behavior macro not found in map!");
            let message = Message::new(
                &msg,
                MessageType::Error,
                MessageLocation::File(file, MessageSpan::new(code_str, code_span.start()), Some((code_span.start(), code_span.end()))),
            );
            message.print();

            return Err(());
        }

        return Ok(());
    }

    fn process_math(&self, expr: Pair<'a, Rule>, file: &str, scope: &str, code_str: &str, code_span: pest::Span<'a>) -> Result<u8, ()> {
        match expr.as_rule() {
            Rule::math => {
                let pairs: Vec<Pair<'a, Rule>> = expr.into_inner().into_iter().collect();

                let mut args: [u8; 2] = [0, 0];

                for i in 0..2 {
                    args[i] = {
                        let a = pairs.get({
                            if i == 0 {
                                0
                            }

                            else {
                                2
                            }
                        }).unwrap();

                        match a.as_rule() {
                            Rule::number => {
                                a.as_str().trim().parse().unwrap()
                            },
                            Rule::variable => {
                                match self.get_const(scope, a.as_str()) {
                                    Some(s) => s,
                                    None => {
                                        let msg = format!("Failed to get constant value for: '{}'", a.as_str());
                                        let message = Message::new(
                                            &msg,
                                            MessageType::Error,
                                            MessageLocation::File(file, MessageSpan::new(code_str, code_span.start()), Some((a.as_span().start(), a.as_span().end()))),
                                        );
                                        message.print();

                                        return Err(());
                                    },
                                }
                            },
                            Rule::math => {
                                self.process_math(a.clone(), file, scope, code_str, code_span)?
                            },
                            _ => unreachable!(),
                        }
                    };
                }

                let arg_1 = args[0];
                let arg_2 = args[1];

                let op = pairs.get(1).unwrap().as_str();

                return Ok(match op {
                    "+" => arg_1.overflowing_add(arg_2).0,
                    "-" => arg_1.overflowing_sub(arg_2).0,
                    "*" => arg_1.overflowing_mul(arg_2).0,
                    "/" => arg_1.overflowing_div(arg_2).0,
                    "%" => arg_1.overflowing_rem_euclid(arg_2).0,
                    "+|" => arg_1.saturating_add(arg_2),
                    "-|" => arg_1.saturating_sub(arg_2),
                    "*|" => arg_1.saturating_mul(arg_2),
                    "/|" => arg_1.saturating_div(arg_2),
                    _ => unreachable!(),
                });
            },
            _ => unreachable!(),
        };
    }

    fn process_condition(&self, cond: Pair<'a, Rule>, file: &str, scope: &str, code_str: &str, code_span: pest::Span<'a>) -> Result<bool, ()> {
        let pairs: Vec<Pair<'a, Rule>> = cond.into_inner().into_iter().collect();

        if pairs.len() == 3 {
            let arg_1: u8 = {
                let a = pairs.get(0).unwrap();

                match a.as_rule() {
                    Rule::condition => {
                        let boolean = self.process_condition(a.clone(), file, scope, a.as_str(), a.as_span())?;
                        let boolean = Box::leak(Box::new((boolean as u8).to_string()));

                        BronoParser::parse(Rule::number, boolean).unwrap().next().unwrap()
                    },
                    Rule::math => {
                        let number = self.process_math(a.clone(), file, scope, code_str, code_span)?;
                        let number = Box::leak(Box::new(number.to_string()));

                        BronoParser::parse(Rule::number, number).unwrap().next().unwrap()
                    },
                    Rule::bool => {
                        let boolean = Self::parse_bool(a.as_str());
                        let boolean = Box::leak(Box::new((boolean as u8).to_string()));

                        BronoParser::parse(Rule::number, boolean).unwrap().next().unwrap()
                    },
                    Rule::variable => {
                        let name = a.as_str();

                        match self.get_const(scope, name) {
                            Some(s) => {
                                let s_string = Box::leak(Box::new(s.to_string()));
                                BronoParser::parse(Rule::number, s_string).unwrap().next().unwrap()
                            },
                            None => {
                                let msg = format!("Inside condition, failed to get value for constant: '{name}'");
                                let message = Message::new(
                                    &msg,
                                    MessageType::Error,
                                    MessageLocation::File(file, MessageSpan::new(code_str, code_span.start()), Some((a.as_span().start(), a.as_span().end())))
                                );
                                message.print();

                                return Err(());
                            },
                        }
                    },
                    _ => a.clone(),
                }
            }.as_str().trim().parse().unwrap();
            let arg_2: u8 = {
                let a = pairs.get(2).unwrap();

                match a.as_rule() {
                    Rule::condition => {
                        let boolean = self.process_condition(a.clone(), file, scope, a.as_str(), a.as_span())?;
                        let boolean = Box::leak(Box::new((boolean as u8).to_string()));

                        BronoParser::parse(Rule::number, boolean).unwrap().next().unwrap()
                    },
                    Rule::math => {
                        let number = self.process_math(a.clone(), file, scope, code_str, code_span)?;
                        let number = Box::leak(Box::new(number.to_string()));

                        BronoParser::parse(Rule::number, number).unwrap().next().unwrap()
                    },
                    Rule::bool => {
                        let boolean = Self::parse_bool(a.as_str());
                        let boolean = Box::leak(Box::new((boolean as u8).to_string()));

                        BronoParser::parse(Rule::number, boolean).unwrap().next().unwrap()
                    },
                    Rule::variable => {
                        let name = a.as_str();

                        match self.get_const(scope, name) {
                            Some(s) => {
                                let s_string = Box::leak(Box::new(s.to_string()));
                                BronoParser::parse(Rule::number, s_string).unwrap().next().unwrap()
                            },
                            None => {
                                let msg = format!("Inside condition, failed to get value for constant: '{name}'");
                                let message = Message::new(
                                    &msg,
                                    MessageType::Error,
                                    MessageLocation::File(file, MessageSpan::new(code_str, code_span.start()), Some((a.as_span().start(), a.as_span().end())))
                                );
                                message.print();

                                return Err(());
                            },
                        }
                    },
                    _ => a.clone(),
                }
            }.as_str().trim().parse().unwrap();

            let op = pairs.get(1).unwrap().as_str();

            return Ok(match op {
                "==" => { arg_1 == arg_2 },
                ">=" => { arg_1 >= arg_2 },
                "<=" => { arg_1 <= arg_2 },
                "!=" => { arg_1 != arg_2 },
                ">" => { arg_1 > arg_2 },
                "<" => { arg_1 < arg_2 },
                _ => unreachable!(),
            });
        }

        else if pairs.len() == 1 {
            let arg = pairs.get(0).unwrap();

            match arg.as_rule() {
                Rule::bool => return Ok(Self::parse_bool(arg.as_str())),
                _ => unreachable!(),
            };
        }

        else {
            unreachable!();
        }
    }

    fn parse_bool(boolean: &str) -> bool {
        return match boolean.trim() {
            "true" | "yes" => true,
            "false" | "no" => false,
            _ => unreachable!(),
        };
    }

    fn process_function(&mut self, file: &str, scope: &str, arguments: &Vec<u8>, code: &Vec<Pair<'a, Rule>>) -> Result<(), ()> {
        for (i, n) in arguments.iter().zip(self.functions.get(scope).unwrap().0.clone()) {
            self.register_const(scope, n, *i);
        }

        self.process_block(file, scope, code, false)?;

        self.close_scope(scope);

        return Ok(());
    }

    fn process_block(&mut self, file: &str, scope: &str, code: &Vec<Pair<'a, Rule>>, new_scope: bool) -> Result<(), ()> {
        let mut scope_string = scope.to_string();

        if new_scope {
            scope_string = inc_scope_depth(scope);
        }

        let scope = &scope_string;

        for (i, c) in code.into_iter().enumerate() {
            self.process_block_line(file, scope, c.clone(), code.get(i + 1))?;
        }

        if new_scope {
            self.close_scope(scope);
        }

        return Ok(());
    }

    // Process code that belongs inside code blocks.
    fn process_block_line(&mut self, file: &str, scope: &str, code: Pair<'a, Rule>, next: Option<&Pair<'a, Rule>>) -> Result<(), ()> {
        if self.skip_next {
            self.skip_next = false;
            return Ok(());
        }

        let code_rule = code.as_rule();
        let code_str = code.as_str();
        let code_str_len = code_str.chars().into_iter().collect::<Vec<char>>().len();
        let code_span = code.as_span();

        let pairs: Vec<_> = code.into_inner().into_iter().collect();

        match code_rule {
            Rule::k_bf => {
                let brainfuck_code = pairs.get(0).unwrap().as_str();

                for i in brainfuck_code.chars() {
                    self.bf_out.push(i);
                }
            },
            Rule::k_const => {
                let name = pairs.get(0).unwrap().as_str();
                let value_pair = pairs.get(1).unwrap();

                let value: u8 = match value_pair.as_rule() {
                    Rule::number => value_pair.as_str().trim().parse().unwrap(),
                    Rule::math => self.process_math(value_pair.clone(), file, scope, code_str, code_span)?,
                    _ => unreachable!(),
                };

                self.register_const(scope, name, value);
            },
            Rule::k_deconst => {
                let name = pairs.get(0).unwrap();

                let const_scope = match self.get_const_scope(scope, name.as_str()) {
                    Some(s) => s,
                    None => {
                        let msg = format!("Could not get scope for constant: {}", name.as_str());
                        let message = Message::new(
                            &msg,
                            MessageType::Error,
                            MessageLocation::File(
                                file,
                                MessageSpan::new(code_str, code_span.start()), Some((name.as_span().start(), name.as_span().start() + name.as_str().chars().count())),
                            ),
                        );
                        message.print();

                        return Err(());
                    },
                };

                if const_scope.trim() == "" {
                    let msg = format!("Cannot delete global constant!");
                    let message = Message::new(
                        &msg,
                        MessageType::Error,
                        MessageLocation::File(
                            file,
                            MessageSpan::new(code_str, code_span.start()), Some((name.as_span().start(), name.as_span().start() + name.as_str().chars().count())),
                        ),
                    );
                    message.print();

                    return Err(());
                }

                self.consts.get_mut(&const_scope).unwrap().remove(name.as_str()).unwrap();
            },
            Rule::k_reconst => {
                let name = pairs.get(0).unwrap();
                let value = pairs.get(1).unwrap();

                let const_scope = match self.get_const_scope(scope, name.as_str()) {
                    Some(s) => s,
                    None => {
                        let msg = format!("Could not get scope for constant: {}", name.as_str());
                        let message = Message::new(
                            &msg,
                            MessageType::Error,
                            MessageLocation::File(
                                file,
                                MessageSpan::new(code_str, code_span.start()), Some((name.as_span().start(), name.as_span().start() + name.as_str().chars().count())),
                            ),
                        );
                        message.print();

                        return Err(());
                    },
                };

                if const_scope.trim() == "" {
                    let msg = format!("Cannot change value of global constant!");
                    let message = Message::new(
                        &msg,
                        MessageType::Error,
                        MessageLocation::File(
                            file,
                            MessageSpan::new(code_str, code_span.start()), Some((name.as_span().start(), name.as_span().start() + name.as_str().chars().count())),
                        ),
                    );
                    message.print();

                    return Err(());
                }

                *self.consts.get_mut(&const_scope).unwrap().get_mut(name.as_str()).unwrap() = match value.as_rule() {
                    Rule::number => value.as_str().trim().parse().unwrap(),
                    Rule::math => self.process_math(value.clone(), file, scope, code_str, code_span)?,
                    _ => unreachable!(),
                };
            },
            Rule::fn_call => {
                let name = pairs.get(0).unwrap().as_str();
                let args_raw = pairs.get(1).unwrap().clone().into_inner().into_iter().map(|x| x.as_str()).collect::<Vec<_>>();

                let mut args: Vec<u8> = Vec::new();
                // For whatever reason, no arguments show up as: [""]
                if (args_raw.len() == 1 && args_raw.get(0).unwrap().trim() == "") == false {
                    for i in args_raw {
                        match i.trim().parse::<u8>() {
                            Ok(o) => args.push(o),
                            Err(_) => {
                                let value = match self.get_const(&scope, i) {
                                    Some(s) => s,
                                    None => {
                                        let msg = format!("Failed to get constant value for: '{i}'");
                                        let message = Message::new(
                                            &msg,
                                            MessageType::Error,
                                            MessageLocation::File(file, MessageSpan::new(code_str, code_span.start()), Some((code_span.start(), code_span.start() + code_str_len))),
                                        );
                                        message.print();

                                        return Err(());
                                    },
                                };

                                args.push(value);
                            },
                        };
                    }
                }

                match self.functions.get(name) {
                    Some(function) => {
                        if args.len() != function.0.len() {
                            let p_span_start = code_span.start() + name.chars().into_iter().collect::<Vec<char>>().len();

                            let msg = format!("Expected {} arguments, but got {} instead!", function.0.len(), args.len());
                            let message = Message::new(
                                &msg,
                                MessageType::Error,
                                MessageLocation::File(file, MessageSpan::new(code_str, code_span.start()), Some((p_span_start, code_span.end() - 1))),
                            );
                            message.print();

                            return Err(());
                        }

                        self.process_function(file, name, &args, &function.1.clone())?;
                    },
                    None => {
                        let name_length = name.chars().into_iter().collect::<Vec<char>>().len();

                        let msg = format!("Non-existant function: '{name}'");
                        let message = Message::new(
                            &msg,
                            MessageType::Error,
                            MessageLocation::File(file, MessageSpan::new(code_str, code_span.start()), Some((code_span.start(), code_span.start() + name_length))),
                        );
                        message.print();

                        return Err(());
                    },
                };
            },
            Rule::macro_call => {
                let name = pairs.get(0).unwrap().as_str();
                let args: Vec<_> = pairs.get(1).unwrap()
                    .clone()
                    .into_inner()
                    .into_iter()
                    .map(|x| (x.clone(), x.as_rule()))
                    .map(|x| {
                        if x.1 != Rule::ident {
                            (x.0, MacroArgType::from_rule(x.1).unwrap())
                        }

                        else {
                            if let Some(s) = self.get_const(scope, x.0.as_str()) {
                                let s_string = Box::leak(Box::new(s.to_string()));
                                let parsed = BronoParser::parse(Rule::number, s_string).unwrap().next().unwrap();

                                (parsed, MacroArgType::from_rule(Rule::number).unwrap())
                            }

                            else {
                                (x.0, MacroArgType::from_rule(x.1).unwrap())
                            }
                        }
                    })
                    .collect();

                match self.macros.get(name) {
                    Some(m) => {
                        if args.len() != m.0.len() {
                            let p_span_start = code_span.start() + name.chars().into_iter().collect::<Vec<char>>().len() + 1;

                            let msg = format!("Expected {} arguments, but got {} instead!", m.0.len(), args.len());
                            let message = Message::new(
                                &msg,
                                MessageType::Error,
                                MessageLocation::File(file, MessageSpan::new(code_str, code_span.start()), Some((p_span_start, code_span.end() - 1))),
                            );
                            message.print();

                            return Err(());
                        }

                        for i in 0..args.len() {
                            if args[i].1 != m.0[i].1 {
                                let p_span_start = code_span.start() + name.chars().into_iter().collect::<Vec<char>>().len() + 1;

                                let msg = format!("Macro argument types are incorrect!");
                                let message = Message::new(
                                    &msg,
                                    MessageType::Error,
                                    MessageLocation::File(file, MessageSpan::new(code_str, code_span.start()), Some((p_span_start, code_span.end() - 1))),
                                );
                                message.print();

                                return Err(());
                            }
                        }

                        self.expand_macro(file, scope, &args, name)?;
                    },
                    None => {
                        let name_length = name.chars().into_iter().collect::<Vec<char>>().len();

                        let msg = format!("Non-existant macro: '{name}'");
                        let message = Message::new(
                            &msg,
                            MessageType::Error,
                            MessageLocation::File(file, MessageSpan::new(code_str, code_span.start()), Some((code_span.start(), code_span.start() + name_length))),
                        );
                        message.print();

                        return Err(());
                    },
                };
            },
            Rule::k_loop => {
                let code: Vec<_> = pairs.get(0)
                    .unwrap()
                    .clone()
                    .into_inner()
                    .into_iter()
                    .collect();

                self.bf_out.push('[');
                self.process_block(file, scope, &code, true)?;
                self.bf_out.push(']');
            },
            Rule::b_macro_call => {
                let name = pairs.get(0).unwrap().as_str();

                let bm: BMacro;

                if pairs.len() > 1 {
                    let args: Vec<BMacroArg> = pairs.get(1).unwrap()
                        .clone()
                        .into_inner()
                        .into_iter()
                        .map(|x| match x.as_rule() {
                            Rule::ident => BMacroArg::new(x.as_str().to_string(), BMacroArgType::Ident),
                            Rule::number => BMacroArg::new(x.as_str().to_string(), BMacroArgType::Number),
                            Rule::string => BMacroArg::new(x.as_str().to_string(), BMacroArgType::String),
                            _ => unreachable!(),
                        })
                        .collect();

                    bm = BMacro::Args(name.to_string(), args);
                }

                else {
                    bm = BMacro::NoArgs(name.to_string());
                }

                if let Some(s) = next {
                    self.expand_b_macro(bm, file, scope, code_str, code_span, s)?;
                    self.skip_next = true;
                }

                else {
                    let msg = format!("No code after behavior macro: '{name}'");
                    let message = Message::new(
                        &msg,
                        MessageType::Error,
                        MessageLocation::File(file, MessageSpan::new(code_str, code_span.start()), Some((code_span.start(), code_span.end()))),
                    );
                    message.print();

                    return Err(());
                }
            },
            Rule::k_if => {
                let do_it = self.process_condition(pairs.get(0).unwrap().clone(), file, scope, code_str, code_span)?;
                let block: Vec<Pair<'a, Rule>> = pairs.get(1).unwrap().clone().into_inner().into_iter().collect();

                if do_it {
                    self.process_block(file, scope, &block, true)?;
                }

                else {
                    if let Some(s) = pairs.get(2) {
                        self.process_if_variant(s.as_rule(), &s.clone().into_inner().collect(), file, scope, code_str, code_span)?;
                    }
                }
            },
            Rule::block => {
                self.process_block(file, scope, &pairs, true)?;
            },
            _ => {
                let span_text = code_str;
                let span_start = code_span.start();
                let span_end = code_span.end();

                let msg = format!("Unimplemented operation inside code block! (OP: {:?})", code_rule);
                let message = Message::new(
                    &msg,
                    MessageType::Warning,
                    MessageLocation::File(file, MessageSpan::new(span_text, span_start), Some((span_start, span_end))),
                );
                message.print();
            },
        };

        return Ok(());
    }

    fn process_if_variant(&mut self, rule: Rule, pairs: &Vec<Pair<'a, Rule>>, file: &str, scope: &str, code_str: &str, code_span: pest::Span<'a>) -> Result<(), ()> {
        match rule {
            Rule::k_elif => {
                let do_it = self.process_condition(pairs.get(0).unwrap().clone(), file, scope, code_str, code_span)?;
                let block = pairs.get(1).unwrap().clone();

                if do_it {
                    self.process_block(file, scope, &block.into_inner().collect(), true)?;
                }

                else {
                    if let Some(s) = pairs.get(2) {
                        self.process_if_variant(s.as_rule(), &s.clone().into_inner().collect(), file, scope, code_str, code_span)?;
                    }
                }
            },
            Rule::k_else => {
                let block = pairs.get(0).unwrap().clone();

                self.process_block(file, scope, &block.into_inner().collect(), true)?;
            },
            _ => unreachable!(),
        };

        return Ok(());
    }

    fn close_scope(&mut self, scope: &str) {
        if let Some(s) = self.consts.get_mut(scope) {
            s.clear();
        }
    }

    fn register_const<S: ToString>(&mut self, scope: &str, name: S, value: u8) {
        if self.consts.contains_key(scope) == false {
            self.consts.insert(scope.to_string(), HashMap::new());
        }

        self.consts.get_mut(scope).unwrap().insert(name.to_string(), value);
    }

    fn get_const_scope(&self, most_in_scope: &str, name: &str) -> Option<String> {
        let scope_depth = get_scope_depth(most_in_scope);

        let result = match self.consts.get(most_in_scope) {
            Some(s) => {
                match s.get(name) {
                    Some(s) => Some(s),
                    None => None,
                }
            },
            None => None,
        };

        match result {
            Some(_) => {
                return Some(most_in_scope.to_string());
            },
            None => {
                match scope_depth {
                    0 => {
                        if most_in_scope.trim() == "" {
                            return None;
                        }

                        else {
                            return self.get_const_scope("", name);
                        }
                    },
                    _ => return self.get_const_scope(&dec_scope_depth(most_in_scope), name),
                }
            },
        };
    }

    fn get_const(&self, scope: &str, name: &str) -> Option<u8> {
        return Some(*self.consts.get(&self.get_const_scope(scope, name)?).unwrap().get(name).unwrap());
    }
}

fn dec_scope_depth(scope: &str) -> String {
    let old_depth = get_scope_depth(scope);
    let zero_depth = get_zero_depth_scope(scope);

    if old_depth == 1 {
        return zero_depth;
    }

    return format!("{}{zero_depth}", old_depth - 1);
}

fn inc_scope_depth(scope: &str) -> String {
    let old_depth = get_scope_depth(scope);
    let zero_depth = get_zero_depth_scope(scope);

    return format!("{}{zero_depth}", old_depth + 1);
}

fn get_zero_depth_scope(scope: &str) -> String {
    let mut new_scope = String::new();

    for c in scope.chars() {
        if c.is_numeric() == false {
            new_scope.push(c);
        }
    }

    return new_scope;
}

fn get_scope_depth(scope: &str) -> usize {
    let mut number = String::new();

    for c in scope.chars() {
        if c.is_numeric() {
            number.push(c);
        }

        else {
            break;
        }
    }

    if number.trim() == "" {
        return 0;
    }

    else {
        return number.trim().parse().unwrap();
    }
}

fn b_macro_map<'a>() -> HashMap<String, Vec<BMacroArgType>> {
    let mut map: HashMap<String, _> = HashMap::new();

    map.insert("rep".into(), vec![BMacroArgType::Number]);
    map.insert("iter_text".into(), vec![BMacroArgType::String, BMacroArgType::Ident]);

    return map;
}

fn b_macro_func<'a>(p: &mut Vec<Pair<'a, Rule>>, n: &Pair<'a, Rule>, a: Vec<BMacroArg>, m_name: &str) {
    match m_name.trim() {
        "rep" => b_macro_func::rep(p, n, a),
        "iter_text" => b_macro_func::iter_text(p, n, a),
        _ => unreachable!(),
    };
}

fn brono_ast_static_str(rule: Rule, input_str: &'static str) -> Result<Pairs<'static, Rule>, pest::error::Error<Rule>> {
    return BronoParser::parse(rule, input_str);
}
