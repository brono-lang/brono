use std::fs;
use brono::*;

fn main() {
    let code = fs::read_to_string("parsing_tests/1.brono").unwrap();

    let mut compiler = Compiler::new("test.brono", &code);

    let bf = compiler.compile().unwrap();

    println!("{}", bf);
}
